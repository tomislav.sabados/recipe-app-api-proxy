# Recipe app api proxy

NGINX proxy app for our recipe app API

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen to (default: `8000`)
* `APP_HOST` - Hostname of athe app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requsts to (default: `9000`)